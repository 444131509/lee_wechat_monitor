/**
 * 
 */
package per.lxy.wechat;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import per.lxy.wechat.net.JpcapPacket;
import per.lxy.wechat.net.WinNetworkApi;
import per.lxy.wechat.net.impl.WinNetworkApiImpl;


/**
 * @author lixiaoying
 *
 */
public class Main {
	
	private static final List<String> PROCESS_NAME_SET = Arrays.asList("WeChat");
	
	public static void main(String[] args)  {
		WinNetworkApi winNetworkApi = new WinNetworkApiImpl();
		try {
			//查找进程的服务端IP
			Set<String> notIgnoreAddress =  winNetworkApi.findTarServerIpByName(PROCESS_NAME_SET);
			System.out.println("感兴趣IP:"+notIgnoreAddress);
			//开始监听感兴趣的目标ip的数据包
			JpcapPacket packet = new JpcapPacket();
			packet.setNotIgnoreAddress(notIgnoreAddress);
			packet.start();
		} catch (IOException e) {
			e.printStackTrace();
		}
//		(ip.dst eq 163.177.90.84 && ip.src eq 192.168.199.124) || (ip.dst eq 163.177.81.141 && ip.src eq 192.168.199.124)
	}
}
