/**
 * 
 */
package per.lxy.wechat.net;

import java.io.IOException;
import java.util.Set;

import jpcap.JpcapCaptor;
import jpcap.NetworkInterface;
import jpcap.PacketReceiver;
import jpcap.packet.IPPacket;
import jpcap.packet.Packet;
import per.lxy.wechat.util.MyStringUtils;

/***
 * 监听网络包
 */
public class JpcapPacket {

	// 感兴趣的IP
	private Set<String> notIgnoreAddress;

	public void start() {
		/*--------------    第一步绑定网络设备       --------------*/
		NetworkInterface[] devices = JpcapCaptor.getDeviceList();
		int caplen = 102400;
		boolean promiscCheck = true;
		for (NetworkInterface n : devices) {
			System.out.println(n.name + "     |     " + n.description);

			new Thread(() -> {
				JpcapCaptor jpcap;
				try {
					jpcap = JpcapCaptor.openDevice(n, caplen, promiscCheck, 50);
					// jpcap.setFilter("tcp", true); // 设置过滤规则，只抓取tcps数据包
					jpcap.loopPacket(-1, new MessageReceive());
				} catch (IOException e) {
					e.printStackTrace();
				}

			}).start();

		}
		System.out.println("-------------------------------------------");

	}

	class MessageReceive implements PacketReceiver {
		@Override
		public void receivePacket(Packet packet) {
			IPPacket ip = (IPPacket) packet;// 强转
			if (isIgnore(ip)) {
				return;
			}
			String protocol = "";

			switch (new Integer(ip.protocol)) {
			case 1:
				protocol = "ICMP";
				break;
			case 2:
				protocol = "IGMP";
				break;
			case 6:
				protocol = "TCP";
				break;
			case 8:
				protocol = "EGP";
				break;
			case 9:
				protocol = "IGP";
				break;
			case 17:
				protocol = "UDP";
				break;
			case 41:
				protocol = "IPv6";
				break;
			case 89:
				protocol = "OSPF";
				break;
			default:
				break;
			}

			println(ip, protocol);
			// if (packet instanceof IPPacket && ((IPPacket) packet).version == 4) {
			//
			// }
		}
	}

	/**
	 * 判断是否感兴趣的ip
	 */
	private boolean isIgnore(IPPacket ip) {
		return !notIgnoreAddress.contains(ip.src_ip.getHostAddress())
				&& !notIgnoreAddress.contains(ip.dst_ip.getHostAddress());
	}

	private void println(IPPacket ip, String protocol) {

		/* IP数据报文数据 */
		String text = MyStringUtils.bytesToHexString(ip.data);
		System.out.println(text);

//		System.out.println("版本：IPv4");
//		System.out.println("优先权：" + ip.priority);
		// System.out.println("区分服务：最大的吞吐量： " + ip.t_flag);
		// System.out.println("区分服务：最高的可靠性：" + ip.r_flag);
		System.out.println("长度：" + ip.length);
		// System.out.println("标识：" + ip.ident);
		// System.out.println("DF:Don't Fragment: " + ip.dont_frag);
		// System.out.println("NF:Nore Fragment: " + ip.more_frag);
//		System.out.println("片偏移：" + ip.offset);
//		System.out.println("生存时间：" + ip.hop_limit);
		System.out.println("协议：" + protocol);
		// System.out.println("源IP " + ip.src_ip.getHostAddress());
		System.out.println("目的IP " + ip.dst_ip.getHostAddress());
		// System.out.println("源主机名： " + ip.src_ip);// =/[ip]
		// System.out.println("目的主机名： " + ip.dst_ip);// =/[ip]

		System.out.println("----------------------------------------------");
	}

	public void setNotIgnoreAddress(Set<String> notIgnoreAddress) {
		this.notIgnoreAddress = notIgnoreAddress;
	}

}