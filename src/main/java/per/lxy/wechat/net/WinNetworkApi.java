/**
 * 
 */
package per.lxy.wechat.net;

import java.io.IOException;
import java.util.List;
import java.util.Set;

/**
 * @author lixiaoying
 *
 */
public interface WinNetworkApi {

	/**
	 * 查找进程ID
	 * [8072, 10776]
	 * */
	Set<String> findPidByName(List<String> processName) throws IOException;

	/**
	 * 查找进程使用的服务端ip
	 * return [14.17.43.152:80, 180.163.25.150:80]
	 * */
	Set<String> findTarServerIpByName(List<String> processName) throws IOException;

}