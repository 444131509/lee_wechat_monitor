```

ip报文结构

https://www.cnblogs.com/kakaxisir/p/4187706.html

版本（4bit）	报头长度（4bit）	优先级和服务类型（8bit）	总长度（16bit）
标识（16bit）	标志（3bit）	分段偏移（13bit）
存活期（8bit）	协议（8bit）	报头校验和（16bit）
源IP地址（32bit）
目的IP地址（32bit）
选项（0或32bit，若有的话）
数据（可变）

版本 IP版本号。
报头长度 32位字的报头长度(HLEN)。
优先级和服务类型 服务类型描述数据报将如何被处理。前3位表示优先级位。
总长度 包括报头和数据的数据包长度。
标识 唯一的IP数据包值。
标志 说明是否有数据被分段。
分段偏移 如果数据包在装人帧时太大,则需要进行分段和重组。分段功能允许在因特网上存在有大小不同的最大传输单元(MUT)。
存活期(TTL) 存活期是在数据包产生时建立在其内部的一个设置。如果这个数据包在这个TTL到期时仍没有到达它要去的目的地,那么它将被丢弃。这个设置将防止IP包在寻找目的地的时候在网络中不断循环。
协议 上层协议的端口(TCP是端口6;UDP是端口17(十六进制)) 。同样也支持网络层协议,如ARP和ICMP。在某些分析器中被称为类型字段。下面将给出这个字段更详细的说明。
报头校验和 只针对报头的循环冗余校验(CRC)。
源IP地址 发送站的32位IP地址。
目的IP地址 数据包目的方站点的32位IP地址。
选项 用于网络检测、调试、安全以及更多的内容。
数据 在IP选项字段后面的就是上层数据。


				/** Protocol number for ICMP */
  　　　　　　　　  public static final short IPPROTO_ICMP = 1;

 　　　　　　　　   /** Protocol number for IGMP */
　　　　　　　　    public static final short IPPROTO_IGMP = 2;

 　　　　　　　　   /** Protocol number for IP in IP */
　　　　　　　　    public static final short IPPROTO_IP = 4;

 　　　　　　　　   /** Protocol number for TCP */
　　　　　　　　    public static final short IPPROTO_TCP = 6;

　　　　　　　　    /** Protocol number for UDP */
　　　　　　　　    public static final short IPPROTO_UDP = 17;

  　　　　　　　　  /** Protocol number for IPv6 */
 　　　　　　　　   public static final short IPPROTO_IPv6 = 41;

 　　　　　　　　   /** Protocol number for IPv6 hop-by-hop option */
 　　　　　　　　   public static final short IPPROTO_HOPOPT = 0;

  　　　　　　　　  /** Protocol number for routing header for IPv6 */
 　　　　　　　　   public static final short IPPROTO_IPv6_Route = 43;

　　　　　　　　    /** Protocol number for fragment header for IPv6 */
　　　　　　　　    public static final short IPPROTO_IPv6_Frag = 44;

  　　　　　　　　  /** Protocol number for IPv6 ICMP */
  　　　　　　　　  public static final short IPPROTO_IPv6_ICMP = 58;

　　　　　　　　    /** Protocol number for no next header header for IPv6 */
 　　　　　　　　   public static final short IPPROTO_IPv6_NoNxt = 59;

  　　　　　　　　  /** Protocol number for destination option for IPv6 */
 　　　　　　　　   public static final short IPPROTO_IPv6_Opts = 60;
 
```